import functools
from flask import Flask , request , jsonify
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity
from functools import wraps

app = Flask(__name__)

# Configuración JWT

app.config['JWT_SECRET_KEY'] = 'aqFU7e_V7AJ9Hd8' 
jwt = JWTManager(app)

# Fim de Generación de llave, puede cambiar el JWT_SECRET_KEY por lo que quieran

def check_auth(username, password):
    return username == "admin" and password == "admin123"


def login_required(f):
    @functools.wraps(f)
    def decorated_func(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return (
                "No posee autorización login",
                401,
                {"WWW-Authenticate": "Basic realm='Login Required'"},
            )
        return f(*args, **kwargs)

    return decorated_func


def search_dni(value):
    try:
        with open('./Data/Padron.dat', 'r') as file:
            lines = file.readlines()
            for line in lines:
                datafile = line.split("|")
                if datafile[2] == value:
                    return jsonify({"dni": datafile[2],"apellidos": datafile[3],"nombres": datafile[4],"mesa": datafile[12]}),200
            return 'El valor no fue encontrado en el archivo.',404
    except FileNotFoundError:
        resp = 'Error en Service',501
        return resp
       
@app.route('/')
def hola():
    return "Docker Python Flask Funcionando!"

@app.route('/saludo')
def saludar():
    return "Que tal , como estas!!"

@app.route('/v1/dni', methods=['GET'])
def searchDni():
    value = request.args.get('value')
    if value is None:
        return 'Por favor, proporcionar un valor numérico en el parámetro "value".', 400
    result = search_dni(value)
    return result

@app.route('/v2/dni', methods=['GET'])
@login_required
def searchDniV2():
    value = request.args.get('value')
    if value is None:
        return 'Por favor, proporcionar un valor numérico en el parámetro "value".', 400
    result = search_dni(value)
    return result

# EndPoint segurizado con JWT
# Ruta para iniciar sesión y generar un token Bearer JWT , hacer primero esta pegada para obtnerlo y utilzarlo 
@app.route('/login', methods=['POST'])
def login():
    username = request.json.get('username', None)
    password = request.json.get('password', None)
    
    # En un escenario real, verificarías las credenciales del usuario aquí
    if username == 'admin' and password == 'admin123':
        access_token = create_access_token(identity=username)
        return jsonify(access_token=access_token), 200
    else:
        return jsonify({'message': 'Usuario o contraseña incorrectos'}), 401

# Deben primero hacer la pegada a la ruta /login para obtener el token
@app.route('/v3/dni', methods=['GET'])
@jwt_required()
def searchDniV3():
    value = request.args.get('value')
    if value is None:
        return 'Por favor, proporcionar un valor numérico en el parámetro "value".', 400
    result = search_dni(value)
    return result

@app.route('/v11/dni', methods=['POST'])
def searchDniv4():
    value = request.json.get('dni', None)
    if value is None:
        return 'Por favor, proporcionar un valor numérico en el parámetro "value".', 400
    result = search_dni(value)
    return result